----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: clk_divider - Behavioral
-- Project Name: Contador Sincrono
-- 
----------------------------------------------------------------------------------
--HHH

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_divider is
    Port ( --INPUTS
           clk      : in  STD_LOGIC;                   --Entrada de reloj de la placa.
           freq_sel : in STD_LOGIC_VECTOR(1 downto 0); --Selector de la frecuencia de actualizacion del contador.
           reset_n  : in  STD_LOGIC;                   --Reseteo del modulo (negado). Reset: 1.
           
           --OUTPUTS
           clk_out : out STD_LOGIC); --Frecuencia de reloj deseada.
end entity;

architecture Behavioral of clk_divider is
    --SIGNALS
	signal clk_sig : STD_LOGIC;   --Se�al de reloj que se desea conseguir.
	signal limit_value : integer; --Valor limite del reloj que produce el flanco.

begin
    with freq_sel select
        limit_value <= 99999999 when "00", --Cada 2s. Frecuencia de 0.5 Hz.
                       49999999 when "01", --Cada 1s. Frecuencia de 1 Hz.
                       24999999 when "10", --Cada 500ms. Frecuencia de 2 Hz.
                        1599999/20 when others; --Opcion exclusiva para la frecuencia de refresco del display (50 Hz). 1599999
    
  --TESTBENCH    
--    with freq_sel select
--        limit_value <= 24 when "00",
--                       12 when "01",
--                       6 when "10",
--                       2 when others;
                       
    p0 : process(reset_n, clk)
        variable cnt : integer; --Contador que suma para luego compararlo con el valor limite.
        
     begin
        --RESET PRIORITARIO
        if (reset_n = '1') then
            clk_sig <= '0';
            cnt := 0;
        
        --FLANCO DE SUBIDA DE CLK
        elsif rising_edge(clk) then
            if (cnt = limit_value) then --Cuando la cuenta alcanza el valor limite
               clk_sig <= NOT(clk_sig); --Se cambia la se�al del reloj deseada
               cnt := 0;                --Y se reinicia la cuenta
           else
               cnt := cnt + 1;
           end if;
        end if;
       
     end process;
    
    --ASOCIACION DE SALIDAS
    clk_out <= clk_sig;

end Behavioral;
