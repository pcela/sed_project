----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: tb_debouncing - Behavioral
-- Project Name: Contador Sincrono
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_debouncing is
end tb_debouncing;

architecture Behavioral of tb_debouncing is
component debouncing is
    Port( --INPUTS
		  button : in STD_LOGIC;
		  clk    : in STD_LOGIC;
		
		  --OUTPUTS
		  result : out STD_LOGIC);		  

end component;

--SIGNALS
signal button : STD_LOGIC := '0';
signal clk    : STD_LOGIC := '0';
signal result : STD_LOGIC := '0';

begin
	deb: debouncing
		Port map( button => button,
		          clk    => clk,   
		          result => result);
		
		process
		begin
			clk <= NOT clk;
			wait for 25 ns;
		end process;
		
		p0: process
		begin
			button <= '0';
			for i in 1 to 3 loop
				button <= NOT button;
				wait for 5 ns;
			end loop;
			
			button <= '1';
			wait for 500 ns;
			
			
			wait;
			assert result = '1'
				report "Algo ha salido mal."
				severity note;
				
			assert false
				report "Simulacion finalizada. Test superado."
				severity note;
				
		end process;

end Behavioral;
