----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: tb_main_source - Behavioral
-- Project Name: Contador Sincrono
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_main_source is
end tb_main_source;

architecture Behavioral of tb_main_source is
component main_source is
	Port (--INPUTS
		  clk		   : in STD_LOGIC; --Se�al de reloj de la placa
		  freq_sel     : in STD_LOGIC_VECTOR (1 downto 0); --Selector de frecuencia de reloj
		  reset		   : in STD_LOGIC; --Se�al de reset
		  button_enable_count : in STD_LOGIC; --Habilitar/dehabilitar la cuenta
		  button_counting_way : in STD_LOGIC; --Sentido de la cuenta (ascendente/descendente)
		  
		  --OUTPUTS
		  display_number     : out STD_LOGIC_VECTOR (6 downto 0);  --Numero mostrado en el display
          display_selection  : out STD_LOGIC_VECTOR (7 downto 0)); --Selector del display de 7 segmentos
end component;

signal clk		         : STD_LOGIC := '0';
signal freq_sel          : STD_LOGIC_VECTOR (1 downto 0);
signal reset	         : STD_LOGIC := '0';
signal enable_count      : STD_LOGIC := '1';
signal counting_way      : STD_LOGIC := '0';
signal display_number    : STD_LOGIC_VECTOR (6 downto 0);
signal display_selection : STD_LOGIC_VECTOR (7 downto 0);
constant CLK_PERIOD : time := 20ns;

begin
	main: main_source
		Port map( clk		          => clk,		         
		          freq_sel            => freq_sel,         
		          reset	              => reset,	        
		          button_enable_count => enable_count,     
		          button_counting_way => counting_way,     
		          display_number      => display_number,   
		          display_selection   => display_selection);
				  
	process
	begin
		clk <= NOT clk;
        wait for 0.5 * CLK_PERIOD;
    end process;
	
	p0: process
	begin
	   wait for 0.25 * CLK_PERIOD;
		reset <= '1';
		enable_count <= '1';
		counting_way <= '1';
		freq_sel <= "00";
		wait for 50 ns;
		
		reset <= '0';
		wait for 4000 ns;
		
		freq_sel <= "01";
		wait for 300 ns;
		
		counting_way <= '0';
		wait for 4000 ns;		
		
		wait for 10000 ns;
		
		assert false
		  report "Simulacion finalizada. Test superado."
          severity note;
          
	end process;

end Behavioral;
