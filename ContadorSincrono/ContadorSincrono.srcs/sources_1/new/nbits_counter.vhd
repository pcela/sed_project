----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: nbits_counter - Behavioral
-- Project Name: Contador Sincrono
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;


entity nbits_counter is
  --limit debe tener el mismo numero de bits que nbits.
  Generic(nbits : positive        := 8;                                 --Numero de bits que representa el conteo
          limit: std_logic_vector := x"2F");                            --Valor maximo que puede tomar la cuenta.
                                                                        --El valor minimo siempre es 0 ya que no se toman en cuenta valores negativos

  Port (--INPUTS
        clk           : in  STD_LOGIC;                                  --Se�al de reloj a la frecuencia de la placa
        reset         : in  STD_LOGIC;                                  --Reset
        enable        : in  STD_LOGIC;                                  --Activacion de la cuenta: 1 activa el conteo, 0 deshabilita el conteo
        counting_way  : in  STD_LOGIC;                                  --Sentido ascendente/descendente de la cuenta: 1 ascendente, 0 descendente
        
        --OUTPUTS
        COUNT         : out STD_LOGIC_VECTOR(nbits-1 downto 0));        --Valor acumulativo del conteo de tama�o tantos bits como se determine en la variable nbits

end entity;

architecture Behavioral of nbits_counter is
    --SIGNALS
    signal count_i : std_logic_vector(nbits - 1 downto 0) := (others => '0');
    
    --CONSTANTS
    constant zeros : std_logic_vector(nbits - 1 downto 0) := (others => '0');
    
begin
    p0 : process(reset, enable, clk, counting_way)
    begin
        --Se prioriza la se�al de reset.
        if reset = '1' then
           count_i <= (others => '0');
        
        --Con cada flanco positivo del reloj, si la se�al de enable est� activa, se cuenta.   
        elsif rising_edge (clk) then  
          if enable = '1' then
            --Sentido de la cuenta ascendente (incremental)
            if counting_way = '1' then
                
                if count_i = limit then count_i <= (others => '0'); --Se prioriza que la cuenta llegue al limit para reiniciarla a cero.
                else count_i <= count_i + 1;                        --En caso contrario, se aumenta en una unidad.
                end if;
                
            --Sentido de la cuenta descendente (decremental)                  
            elsif counting_way = '0' then
            
                if count_i = zeros then count_i <= limit; --Se prioriza que la cuenta llegue a cero para reiniciarla al valor limit.
                else count_i <= count_i - 1;              --En caso contrario, se disminuye en una unidad.
                end if;
                
            end if;
          else
            report "Enable a 0."
            severity NOTE;
           end if; 
        end if;
   end process;
    
   --Valor final de la cuenta
   COUNT <= count_i;

end Behavioral;

