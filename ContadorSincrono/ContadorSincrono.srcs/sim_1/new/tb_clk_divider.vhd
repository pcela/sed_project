----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: tb_clk_divider - Behavioral
-- Project Name: Contador Sincrono
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_clk_divider is
end tb_clk_divider;

architecture Behavioral of tb_clk_divider is
component clk_divider is
    Port ( --INPUTS
           clk      : in STD_LOGIC;                    --Entrada de reloj de la placa.
           freq_sel : in STD_LOGIC_VECTOR (1 downto 0); --Selector de la frecuencia de actualizacion del contador.
           reset_n  : in STD_LOGIC;                    --Reseteo del modulo (negado). Reset: 1.
           
           --OUTPUTS
           clk_out : out STD_LOGIC); --Frecuencia de reloj deseada.
end component;

--SIGNALS
signal clk      : STD_LOGIC := '0';
signal freq_sel : STD_LOGIC_VECTOR (1 downto 0) := "00";
signal reset_n  : STD_LOGIC;
signal clk_out  : STD_LOGIC;

begin
    div: clk_divider
        Port map ( clk => clk,
                   freq_sel => freq_sel,
                   reset_n => reset_n,
                   clk_out => clk_out);
                   
    process
    begin
        clk <= NOT clk;
        wait for 25 ns;
    end process;
    
    p0 : process
    begin
        reset_n <= '1';        
        wait for 50 ns;
        
        reset_n <= '0';
        freq_sel <= "00";
        wait for 10000 ns;
        
        freq_sel <= "01";
        wait for 10000 ns;
        
        freq_sel <= "10";
        wait for 10000 ns;

        freq_sel <= "11";
        wait for 10000 ns;
       
        assert false
            report "Simulacion finalizada. Test superado."
            severity note;
            
    end process;

end Behavioral;