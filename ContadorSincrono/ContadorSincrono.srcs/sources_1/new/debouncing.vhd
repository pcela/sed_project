----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: debouncing - Behavioral
-- Project Name: Contador Sincrono
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

entity debouncing is
    Port( --INPUTS
		  button : in STD_LOGIC;
		  clk    : in STD_LOGIC;
		
		  --OUTPUTS
		  result : out STD_LOGIC);		  

end entity;

architecture Behavioral of debouncing is
--FLIP-FLOP SIGNALS
signal ff1 : STD_LOGIC := '0'; --Flip-flop 1
signal ff2 : STD_LOGIC := '0'; --Flip-flop 2
signal ff3 : STD_LOGIC := '0'; --Flip-flop 3
signal enable_ff3 : STD_LOGIC := '0'; --Habilita el flip-flop 3.

--COUNTER SIGNALS
signal sclr      : STD_LOGIC;
signal count_i   : unsigned (3 downto 0) := (others => '0');
--STD_LOGIC_VECTOR (3 downto 0) := (others => '0');

--CONSTANTS
constant end_count : unsigned (3 downto 0) := (others => '1');
--STD_LOGIC_VECTOR (3 downto 0) := (others => '1');
begin
	p0: process(clk)
	begin
		if rising_edge(clk) then
			ff1 <= button;
			ff2 <= ff1;
			if (enable_ff3 = '1') then
				ff3 <= ff2;				
			end if;					
															
		end if;		
	end process;
	
	cnt: process (sclr, enable_ff3, clk)
	begin
		if (sclr = '1') then
			count_i <= (others => '0');
			
		elsif enable_ff3 = '0' then
		  if rising_edge(clk) then
			count_i <= count_i + 1;
		  end if;
		end if;
	end process;
	
	enable_ff3 <= '1' when count_i = end_count else '0';
	
	sclr <= ff1 XOR ff2;
	
	--ASOCIACION DE SALIDAS
	result <= ff3;

end Behavioral;
