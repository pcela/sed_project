----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: Decoder - Dataflow
-- Project Name: ContadorSincrono
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decoder is
	Port (--INPUTS
	      code : in STD_LOGIC_VECTOR (3 downto 0); --Codigo BCD.
	      
	      --OUTPUTS
	      led  : out STD_LOGIC_VECTOR(6 downto 0)); --Representa los leds que deben iluminarse en un display de 7 segmentos.
end entity;

architecture Dataflow OF decoder is
begin
    with code select
        led <= "0000001" WHEN "0000", --0
               "1001111" WHEN "0001", --1
               "0010010" WHEN "0010", --2
               "0000110" WHEN "0011", --3
               "1001100" WHEN "0100", --4
               "0100100" WHEN "0101", --5
               "0100000" WHEN "0110", --6
               "0001111" WHEN "0111", --7
               "0000000" WHEN "1000", --8
               "0001100" WHEN "1001", --9
               "0001000" WHEN "1010", --A
               "1100000" WHEN "1011", --B
               "0110001" WHEN "1100", --C
               "1000010" WHEN "1101", --D
               "0110000" WHEN "1110", --E
               "0111000" WHEN "1111", --F
               "1111110" WHEN others; --Guion

end Dataflow;
