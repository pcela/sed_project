----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.01.2018 11:22:22
-- Design Name: 
-- Module Name: main_source - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity main_source is
	Port (--INPUTS
		  clk		   : in STD_LOGIC;                     --Se�al de reloj de la placa
		  freq_sel     : in STD_LOGIC_VECTOR (1 downto 0); --Selector de frecuencia de reloj
		  reset		   : in STD_LOGIC;                     --Se�al de reset
		  button_enable_count : in STD_LOGIC;                     --Habilitar/dehabilitar la cuenta
		  button_counting_way : in STD_LOGIC;                     --Sentido de la cuenta (ascendente/descendente)
		  
		  --OUTPUTS
		  display_number     : out STD_LOGIC_VECTOR (6 downto 0);  --Numero mostrado en el display
          display_selection  : out STD_LOGIC_VECTOR (7 downto 0)); --Selector del display de 7 segmentos
end entity;

architecture Behavioral of main_source is
-------------------------------
--        COMPONENTES        --
-------------------------------

--DIVISOR DE FRECUENCIA
component clk_divider is
	Port ( --INPUTS
           clk      : in  STD_LOGIC;                    --Entrada de reloj de la placa.
           freq_sel : in STD_LOGIC_VECTOR (1 downto 0); --Selector de la frecuencia de actualizacion del contador.
           reset_n  : in  STD_LOGIC;                    --Reseteo del modulo (negado). Reset: 1.
           
           --OUTPUTS
           clk_out : out STD_LOGIC); --Frecuencia de reloj deseada.
end component;

--CONTADOR DE 8 BITS
component nbits_counter is
  --limit debe tener el mismo numero de bits que nbits.
  Generic(nbits : positive        := 8;                                 --Numero de bits que representa el conteo
          limit: std_logic_vector := x"2F");                            --Valor maximo que puede tomar la cuenta.
                                                                        --El valor minimo siempre es 0 ya que no se toman en cuenta valores negativos

  Port (--INPUTS
        clk           : in  STD_LOGIC;                                  --Se�al de reloj a la frecuencia de la placa
        reset         : in  STD_LOGIC;                                  --Reset
        enable        : in  STD_LOGIC;                                  --Activacion de la cuenta: 1 activa el conteo, 0 deshabilita el conteo
        counting_way  : in  STD_LOGIC;                                  --Sentido ascendente/descendente de la cuenta: 1 ascendente, 0 descendente
        
        --OUTPUTS
        COUNT         : out STD_LOGIC_VECTOR(nbits-1 downto 0));        --Valor acumulativo del conteo de tama�o tantos bits como se determine en la variable nbits
end component;

--DECODIFICADOR BCD
component decoder is
	Port (--INPUTS
	      code : in STD_LOGIC_VECTOR (3 downto 0); --Codigo BCD.
	      
	      --OUTPUTS
	      led  : out STD_LOGIC_VECTOR(6 downto 0)); --Representa los leds que deben iluminarse en un display de 7 segmentos.
end component;

--ACTUALIZADOR DE DISPLAY
component display_refresh is
    Generic( nsegments : positive := 7);
    
    Port ( --INPUTS
           clk                : in STD_LOGIC;
           segment_unid       : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
           segment_dec        : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
           
           --OUTPUTS
           display_number     : out STD_LOGIC_VECTOR (nsegments-1 downto 0);
           display_selection  : out STD_LOGIC_VECTOR (7 downto 0));
end component;

--DEBOUNCING
component debouncing is
    Port( --INPUTS
		  button : in STD_LOGIC;
		  clk    : in STD_LOGIC;
		
		  --OUTPUTS
		  result : out STD_LOGIC);		  

end component;

-------------------------------
--          SE�ALES          --
-------------------------------

--DIVISOR DE RELOJ
signal clk_out : STD_LOGIC;

--CONTADOR DE 8 BITS
signal enable_count : STD_LOGIC;
signal counting_way : STD_LOGIC;
signal COUNT : STD_LOGIC_VECTOR (7 downto 0);

--DECODIFICADOR DE UNIDADES
signal led_unid : STD_LOGIC_VECTOR (6 downto 0);

--DECODIFICADOR DE DECENAS
signal led_dec : STD_LOGIC_VECTOR (6 downto 0);

--ACTUALIZADOR DE DISPLAY
signal clk_display  : STD_LOGIC;
signal segment_unid : STD_LOGIC_VECTOR (6 downto 0);
signal segment_dec  : STD_LOGIC_VECTOR (6 downto 0);

--DEBOUNCING
signal res_count_way    : STD_LOGIC := '0';
signal res_enable_count : STD_LOGIC := '0';

       
begin
	clk_div_count: clk_divider
		Port map( clk      => clk,      
                  freq_sel => freq_sel, 
                  reset_n  => reset,  
				  clk_out  => clk_out);
				  
	clk_div_display: clk_divider
		Port map( clk      => clk,      
                  freq_sel => "11", 
                  reset_n  => reset,  
				  clk_out  => clk_display);
				  
	counter: nbits_counter
		Port map( clk          => clk_out,          
				  reset        => reset,        
				  enable       => res_enable_count,--enable_count,--res_enable_count
				  counting_way => res_count_way, --counting_way,--res_count_way 
				  COUNT        => COUNT);
				  
	dcd_unid: decoder
		Port map( code => COUNT (3 downto 0), 
	              led  => led_unid);
				  
	dcd_dec: decoder
		Port map( code => COUNT (7 downto 4), 
	              led  => led_dec);

	display: display_refresh
		Port map( clk               => clk_display,
				  segment_unid      => led_unid,
				  segment_dec       => led_dec,
	              display_number    => display_number,    
	              display_selection => display_selection);
	
	
	------------------------------------------------
	-- DEBOUNCING PARA BOTON DE SENTIDO DE CUENTA --
	------------------------------------------------
	deb_counting_way: debouncing
		Port map( button => button_counting_way,
		          clk    => clk,
		          result => res_count_way);
				  
				  
	p0: process(res_count_way)
	begin
		if rising_edge(res_count_way) then
			counting_way <= NOT counting_way;
		else 
		    counting_way <= counting_way;  
		end if;
	end process;

	------------------------------------------------
	--   DEBOUNCING PARA BOTON DE ENABLE CUENTA   --
	------------------------------------------------	
	deb_enable_count: debouncing
		Port map( button => button_enable_count,
		          clk    => clk,
		          result => res_enable_count);
				  
				  
	p1: process(res_enable_count)
	begin
		if rising_edge(res_enable_count) then
			enable_count <= NOT enable_count;
		else 
		    enable_count <= enable_count;
		end if;
	end process;
		
		
end Behavioral;