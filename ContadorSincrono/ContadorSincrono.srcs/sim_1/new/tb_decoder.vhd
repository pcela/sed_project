----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: tb_decoder - Behavioral
-- Project Name: Contador Sincrono
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_decoder is
end entity;

architecture Behavioral of tb_decoder is
component decoder is
    Port (--INPUTS
	      code : in STD_LOGIC_VECTOR (3 downto 0); --Codigo BCD.
	      
	      --OUTPUTS
	      led  : out STD_LOGIC_VECTOR (6 downto 0)); --Representa los leds que deben iluminarse en un display de 7 segmentos.

end component;

--SIGNALS
signal code : STD_LOGIC_VECTOR (3 downto 0);
signal led  : STD_LOGIC_VECTOR (6 downto 0);

--TYPEDEF
type dec_test is record
    code : std_logic_vector(3 DOWNTO 0);
    led  : std_logic_vector(6 DOWNTO 0);
end record;

type dec_test_vector is array (natural range <>) of dec_test;

constant test : dec_test_vector := ((code => "0000", led => "0000001"),
                                    (code => "0001", led => "1001111"),
                                    (code => "0010", led => "0010010"),
                                    (code => "0011", led => "0000110"),
                                    (code => "0100", led => "1001100"),
                                    (code => "0101", led => "0100100"),
                                    (code => "0110", led => "0100000"),
                                    (code => "0111", led => "0001111"),
                                    (code => "1000", led => "0000000"),
                                    (code => "1001", led => "0001100"),
                                    (code => "1010", led => "0001000"),
                                    (code => "1011", led => "1100000"),
                                    (code => "1100", led => "0110001"),
                                    (code => "1101", led => "1000010"),
                                    (code => "1110", led => "0110000"),
                                    (code => "1111", led => "0111000"));
    

begin
    dcd: decoder
        Port map( code => code,
                  led => led);       
    
    tb: process    
    begin
        for i in 0 to test'high loop
            code <= test(i).code;
            wait for 20 ns;
            assert led = test(i).led
                report "Salida incorrecta."
                severity failure;
        end loop;
        
        
        assert false
            report "Simulación finalizada. Test superado."
            severity note;
    end process;

end Behavioral;
