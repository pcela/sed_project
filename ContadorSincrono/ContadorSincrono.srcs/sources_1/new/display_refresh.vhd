----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: display_refresh - Behavioral
-- Project Name: Contador Sincrono
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

entity display_refresh is
    Generic( nsegments : positive := 7);
    
    Port ( --INPUTS
           clk                : in STD_LOGIC;
           segment_unid       : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
           segment_dec        : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
           
           --OUTPUTS
           display_number     : out STD_LOGIC_VECTOR (nsegments-1 downto 0);
           display_selection  : out STD_LOGIC_VECTOR (7 downto 0));
end entity;

architecture Behavioral of display_refresh is
	signal count_i : STD_LOGIC_VECTOR (1 downto 0) := "00";

begin
    --CONTADOR DE REFRESCO
    p0: process(clk)
    begin
        if rising_edge(clk) then
            count_i <= count_i + 1;
        end if;
    end process;   
    
    --ASOCIACION DE CADA SEGMENTO CON SU VALOR CORRESPONDIENTE
    with count_i select
        display_number <= segment_unid when "00", --Unidades
                          segment_dec when "01",  --Decenas
                          (others => '1') when others;
    
    --ASOCIACION DE POSICION DE VALORES
--    with count_i select                    
--        display_selection <= "0001" when "00", --Unidades
--                             "0010" when "01", --Decenas
--                             "0100" when "10", --Centenas
--                             "1000" when others; --Millares
    with count_i select                    
        display_selection <= "11111110" when "00", --Unidades
                             "11111101" when "01", --Decenas
                             "11111011" when "10", --Centenas
                             "11110111" when others; --Millares   
end Behavioral;
