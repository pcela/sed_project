----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: tb_display_refresh - Behavioral
-- Project Name: Contador Sincrono
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_display_refresh is
end tb_display_refresh;

architecture Behavioral of tb_display_refresh is
	component display_refresh is
		Generic( nsegments : positive := 7);
		
		Port ( --INPUTS
			   clk                : in STD_LOGIC;
			   segment_unid       : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
			   segment_dec        : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
			   
			   --OUTPUTS
			   display_number     : out STD_LOGIC_VECTOR (nsegments-1 downto 0);
			   display_selection  : out STD_LOGIC_VECTOR (3 downto 0));
	end component;
	                         
	signal clk               : STD_LOGIC := '0';
	signal segment_unid      : STD_LOGIC_VECTOR (6 downto 0);
	signal segment_dec       : STD_LOGIC_VECTOR (6 downto 0);
	signal display_number    : STD_LOGIC_VECTOR (6 downto 0);
	signal display_selection : STD_LOGIC_VECTOR (3 downto 0);	
	
begin
	disp: display_refresh
		Port map( clk               => clk,              
		          segment_unid      => segment_unid,     
		          segment_dec       => segment_dec,      
		          display_number    => display_number,   
		          display_selection => display_selection);	
		
	process
	begin
		clk <= NOT clk;
        wait for 25 ns;
    end process;
	
	p0 : process
    begin
         
        segment_unid <= "0001111";
		segment_dec  <= "0000010";
		
		if display_selection = "00" then
			assert display_number = segment_unid
				report "Valor incorrecto en las unidades."
				severity failure;
		
		elsif display_selection = "01" then
			assert display_number = segment_dec
				report "Valor incorrecto en las decenas."
				severity failure;
	   end if;
		
		wait for 5000 ns;
       
        assert false
            report "Simulacion finalizada. Test superado."
            severity note;
            
    end process;

end Behavioral;