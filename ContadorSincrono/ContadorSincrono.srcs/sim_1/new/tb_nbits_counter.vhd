----------------------------------------------------------------------------------
-- Company: Universidad Politecnica de Madrid
-- Engineer: Paula Cela Lopez
-- 
-- Design Name: Contador Sincrono Hexadecimal
-- Module Name: tb_nbits_counter - Behavioral
-- Project Name: Contador Sincrono
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_nbits_counter is
end tb_nbits_counter;

architecture Behavioral of tb_nbits_counter is
component nbits_counter is
  --limit debe tener el mismo numero de bits que nbits.
  Generic(nbits : positive        := 8;                                 --Numero de bits que representa el conteo
          limit: std_logic_vector := x"2F");                            --Valor maximo que puede tomar la cuenta.
                                                                        --El valor minimo siempre es 0 ya que no se toman en cuenta valores negativos

  Port (--INPUTS
        clk           : in  STD_LOGIC;                                  --Se�al de reloj a la frecuencia de la placa
        reset         : in  STD_LOGIC;                                  --Reset
        enable        : in  STD_LOGIC;                                  --Activacion de la cuenta: 1 activa el conteo, 0 deshabilita el conteo
        counting_way  : in  STD_LOGIC;                                  --Sentido ascendente/descendente de la cuenta: 1 ascendente, 0 descendente
        
        --OUTPUTS
        COUNT         : out STD_LOGIC_VECTOR(nbits-1 downto 0));        --Valor acumulativo del conteo de tama�o tantos bits como se determine en la variable nbits

end component;

signal clk           : STD_LOGIC := '0';                                
signal reset         : STD_LOGIC := '0';                          
signal enable        : STD_LOGIC := '0';                                 
signal counting_way  : STD_LOGIC := '0';
signal COUNT         : STD_LOGIC_VECTOR(7 downto 0);

begin
    cnt:  nbits_counter 
        Port map ( clk => clk,                              
                   reset => reset,
                   enable => enable,
                   counting_way => counting_way,
                   COUNT => COUNT);
                   
    process
    begin
        clk <= NOT clk;
        wait for 25ns;
    end process;
    
    p0 : process
    begin
        reset <= '1';
        enable <= '1';
        counting_way <= '1';
        
        wait for 50ns;
        reset <= '0';
        assert COUNT /= x"00"
            report "Valor limite inferior."
            severity NOTE;
        
        assert COUNT /= x"2F"
            report "Valor limite superior."
            severity NOTE;            
                   
        wait for 5000ns;
        assert false
            report "Simulaci�n finalizada. Test superado."
            severity failure;

    end process;

end Behavioral;
